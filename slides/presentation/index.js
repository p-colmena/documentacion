// Import React
import React from "react";
// Import Spectacle Core tags
import {
  BlockQuote,
  Cite,
  Code,
  CodePane,
  Deck,
  Heading,
  Image,
  Link,
  List,
  ListItem,
  Quote,
  Slide,
  Text
} from "spectacle";
// Import theme
import createTheme from "spectacle/lib/themes/default";
// Images
import thinking from "../assets/thinking.png";
import check from "../assets/check.svg";
import cross from "../assets/cross.png";
import lenny from "../assets/lenny.gif";
import bioinformatica from "../assets/bioinformatica.png";
import docking from "../assets/docking.png";
import notBad from "../assets/not-bad.png";
import comparacion from "../assets/comparacion.png";
import shocked from "../assets/shocked.png";
import future from "../assets/future.png";
import paralelo from "../assets/paralelo.png";
import celular from "../assets/celular.png";
import server from "../assets/server.png";
import gpu from "../assets/gpu.png";
import acha from "../assets/achar.png";
import bifes from "../assets/demo.jpg";
import reactredux from "../assets/reactredux.png";
import elixirphoenix from "../assets/elixirphoenix.png";
import postgres from "../assets/postgres.png";

// Require CSS
require("normalize.css");

const theme = createTheme({
  primary: "#3C303A",
  secondary: "#E500E5",
  tertiary: "#FFDD0E",
  quaternary: "#CECECE"
}, {
  primary: "Montserrat",
  secondary: "Helvetica"
});

export default class Presentation extends React.Component {
  render() {
    return (
      <Deck transition={ [ "zoom", "slide" ] } transitionDuration={ 500 } theme={ theme }>

        <Slide transition={ [ "zoom" ] } bgColor="primary">
          <Heading size={ 1 } fit caps lineHeight={ 1 } textColor="secondary">
            Colmena
          </Heading>
          <Text textColor="tertiary" size={ 6 }>
            Agustin Garcia Smith
          </Text>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Heading size={ 6 } textColor="primary" padding={ 20 } caps>Que buscaba con el TIP?</Heading>
          <Image src={ thinking } width="50%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Text textColor="primary" padding={ 20 } bold>
            Aprender cosas nuevas
          </Text>
          <Image src={ check } width="50%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Text textColor="primary" padding={ 20 } bold>
            Que lo que haga no quede en la nada
          </Text>
          <Image src={ check } width="50%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Text textColor="primary" padding={ 20 } bold>
            Hacer algo porque "me tengo que recibir"
          </Text>
          <Image src={ cross } width="50%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ lenny } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Heading size={ 4 } textColor="tertiary" padding={ 20 } caps>Bioinformatica</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Heading size={ 4 } textColor="secondary" padding={ 20 } caps>Drug discovery</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Image src={ bioinformatica } width="90%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Image src={ docking } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <CodePane
            theme="dark"
            source={
              "HEADER    OXYGEN TRANSPORT                        07-MAR-84   4HHB              \n"
              + "TITLE     THE CRYSTAL STRUCTURE OF HUMAN DEOXYHAEMOGLOBIN AT 1.74 ANGSTROMS     \n"
              + "TITLE    2 RESOLUTION                                                           \n"
              + "COMPND    MOL_ID: 1;                                                            \n"
              + "COMPND   2 MOLECULE: HEMOGLOBIN (DEOXY) (ALPHA CHAIN);                          \n"
              + "COMPND   3 CHAIN: A, C;                                                         \n"
              + "COMPND   4 ENGINEERED: YES;                                                     \n"
              + "COMPND   5 MOL_ID: 2;                                                           \n"
              + "COMPND   6 MOLECULE: HEMOGLOBIN (DEOXY) (BETA CHAIN);                           \n"
              + "COMPND   7 CHAIN: B, D;                                                         \n"
              + "COMPND   8 ENGINEERED: YES                                                      \n"
              + "SOURCE    MOL_ID: 1;                                                            \n"
              + "SOURCE   2 ORGANISM_SCIENTIFIC: HOMO SAPIENS;                                   \n"
              + "SOURCE   3 ORGANISM_COMMON: HUMAN;                                              \n"
              + "SOURCE   4 ORGANISM_TAXID: 9606;                                                \n"
              + "SOURCE   5 MOL_ID: 2;                                                           \n"
              + "SOURCE   6 ORGANISM_SCIENTIFIC: HOMO SAPIENS;                                   \n"
              + "SOURCE   7 ORGANISM_COMMON: HUMAN;                                              \n"
              + "SOURCE   8 ORGANISM_TAXID: 9606                                                 \n"
              + "KEYWDS    OXYGEN TRANSPORT                                                      \n"
              + "EXPDTA    X-RAY DIFFRACTION                                                     \n"
            }
          />
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Image src={ notBad } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Image src={ comparacion } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Image src={ shocked } width="50%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ future } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Heading size={ 4 } textColor="primary" padding={ 20 } caps>Divide and conquer</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ bioinformatica } width="90%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ paralelo } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ celular } width="70%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ gpu } width="70%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ server } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Heading size={ 1 } textColor="primary" padding={ 20 } caps>?</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Heading size={ 1 } textColor="secondary" padding={ 20 } caps>Que hicimos?</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Image src={ acha } width="50%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Heading size={ 1 } textColor="secondary" padding={ 20 } caps>POC</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Heading size={ 6 } textColor="secondary" caps>Features</Heading>
          <List>
            <ListItem>Descargar pdb's</ListItem>
            <ListItem>Medir la superficie de un pdb</ListItem>
            <ListItem>Calcular los pockets de un pdb</ListItem>
            <ListItem>Bajar a disco el resultado de los anteriores</ListItem>
            <ListItem>Visualizador en 3d de una proteina</ListItem>
          </List>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Image src={ bifes } width="60%"/>
          <Link href="http://localhost:3000" target="_blank" textColor="secondary">A la demo ---></Link>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Heading size={ 1 } textColor="secondary" padding={ 20 } caps>?</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ reactredux } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Heading size={ 1 } textColor="secondary" padding={ 20 } caps>NGL</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ elixirphoenix } width="100%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Heading size={ 1 } textColor="secondary" padding={ 20 } caps>freesasa</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Heading size={ 1 } textColor="secondary" padding={ 20 } caps>fpocket</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="tertiary">
          <Image src={ postgres } width="60%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Heading size={ 1 } textColor="tertiary" padding={ 20 } caps>desafios</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Heading size={ 4 } textColor="primary" padding={ 20 } caps>Bioinformatica</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Image src={ elixirphoenix } width="70%"/>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <CodePane
            padding={ 10 }
            lang="elixir"
            theme="dark"
            source={
              "defmodule TaskTree do \n"
              + "  defstruct [:name, :function, :subtasks] \n"
              + "\n"
              + "  def node(name, function, subtasks \\ []) do \n"
              + "    %TaskTree{name: name, function: function, subtasks: subtasks} \n"
              + "  end \n"
              + "end \n"
            }
          />

          <CodePane
            padding={ 10 }
            lang="elixir"
            theme="dark"
            source={
              "@spec run_tasks(TaskTree, %{}) :: :ok | {:error, any()} \n"
              + "def run_tasks(tasks, params) do \n"
              + "  with {:ok, res} <- tasks.function.(params) do \n"
              + "    tasks.subtasks \n"
              + "    |> Enum.map(fn s -> Task.async(fn -> run_tasks(s, %{tasks.name => res}) end) end) \n"
              + "    |> Enum.map(fn a -> Task.await a end) \n"
              + "    |> Enum.reduce(:ok, fn x, acc -> if (match? {:error, _}, acc), do: acc, else: x end) \n"
              + "  end \n"
              + "end"
            }
          />
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <CodePane
            lang="jsx"
            theme="dark"
            source={
              "class PdbViewerAction extends React.Component {\n"
              + "  componentDidMount() {\n"
              + "    this.setState({\n"
              + "      stage: new window.NGL.Stage(\"pdbPageViewer\")\n"
              + "    });\n"
              + "  }\n"
              + "  loadPdb(success) {\n"
              + "    const presentation = new TaskTypes(success.task).case({\n"
              + "      structure: () => \"cartoon\",\n"
              + "      pockets: () => \"cartoon\",\n"
              + "      pocket: () => \"surface\"\n"
              + "    });\n"
              + "    this.state.stage.loadFile(new Blob([ success.pdb ], { type: 'text/plain' }), {\n"
              + "      ext: 'pdb'\n"
              + "    })\n"
              + "      .then(component => {\n"
              + "        this.state.components.push({ task: success.task, component: component });\n"
              + "        component.addRepresentation(presentation);\n"
              + "        component.autoView()\n"
              + "      });\n"
              + "  }\n"
              + "  render() {\n"
              + "    return (\n"
              + "      ... \n"
              + "      <div style={ { height: '100%' } }>\n"
              + "        <div style={ { height: 'calc(100vh - 4rem)' } } id=\"pdbPageViewer\">\n"
              + "        </div>\n"
              + "      </div>\n"
              + "    );\n"
              + "  }\n"
              + "}\n"
            }
          />
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="secondary">
          <Heading size={ 4 } textColor="primary" padding={ 20 } caps>?</Heading>
        </Slide>

        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Heading size={ 4 } textColor="secondary" padding={ 20 } caps>CHAU</Heading>
        </Slide>


        <Slide transition={ [ "zoom", "slide" ] } bgColor="primary">
          <Heading size={ 1 } fit caps lineHeight={ 1 } textColor="secondary">
            Spectacle Boilerplate
          </Heading>
          <Text margin="10px 0 0" textColor="tertiary" size={ 1 } fit bold>
            open the presentation/index.js file to get started
          </Text>
        </Slide>

        <Slide transition={ [ "fade" ] } bgColor="tertiary">
          <Heading size={ 6 } textColor="primary" caps>Typography</Heading>
          <Heading size={ 1 } textColor="secondary">Heading 1</Heading>
          <Heading size={ 2 } textColor="secondary">Heading 2</Heading>
          <Heading size={ 3 } textColor="secondary">Heading 3</Heading>
          <Heading size={ 4 } textColor="secondary">Heading 4</Heading>
          <Heading size={ 5 } textColor="secondary">Heading 5</Heading>
          <Text size={ 6 } textColor="secondary">Standard text</Text>
        </Slide>
        <Slide transition={ [ "fade" ] } bgColor="primary" textColor="tertiary">
          <Heading size={ 6 } textColor="secondary" caps>Standard List</Heading>
          <List>
            <ListItem>Item 1</ListItem>
            <ListItem>Item 2</ListItem>
            <ListItem>Item 3</ListItem>
            <ListItem>Item 4</ListItem>
          </List>
        </Slide>
        <Slide transition={ [ "fade" ] } bgColor="secondary" textColor="primary">
          <BlockQuote>
            <Quote>Example Quote</Quote>
            <Cite>Author</Cite>
          </BlockQuote>
        </Slide>
      </Deck>
    );
  }
}
